@ECHO OFF

REM =======================================
REM
REM    make.bat  ver. 1.1
REM
REM =======================================
REM
REM [Purpose]
REM   eZplus application compile
REM
REM [Tag List]
REM   ALL        : make appli file
REM   PROGUARD   : make appli file with "proguard.jar"
REM   RETROGUARD : make appli file with "retroguard.jar"
REM   BASE       : compile only
REM   PRE        : preverify many classes
REM   PRO        : excute proguard
REM   RETRO      : excute retroguard 
REM   JAR        : jar compress
REM   DOC        : make the api document
REM   MF         : write a manifest file
REM   JAD        : write a jad file
REM
REM [Derectory]
REM   src        : source files
REM   res        : resource file
REM   classes    : class files
REM   bin        : binary file
REM
REM =======================================


REM =======================
REM    SET ENVIROMENT

SET JAR=sheep.jar
SET JAD=sheep.jad
SET KJX=sheep.kjx
SET MF=MANIFEST.MF

SET APPLI=Sheep(ezPlus)
SET CLASS=Sheep
SET ICON=icon.png
SET VENDOR=GClue, Inc.
SET VERSION=1.0
SET DSIZE=1024
SET DESCRIPTION=

REM SET JAVA_HOME=
SET PROGUARD=C:\i-jade\proguard.jar
SET RETROGUARD=C:\i-jade\retroguard.jar
SET KJXTOOL=C:\ezplusTools\Tools\CmdTool\KJXArchiver.jar
SET BOOT=C:\WTK104\lib\midpapi.zip
SET LIB=c:\ezplusTools\Tools\KDDI-P\kddip.jar
SET PREVERIFY=C:\WTK104\bin\preverify.exe

REM =======================


REM =======================
REM    SELECT ACTION

IF "%1" == "" GOTO ALL
GOTO %1

REM =======================


REM =======================
REM    MAKE BINARY FILE

:ALL

CALL :MF
CALL :BASE
CALL :PRE
CALL :JAR
CALL :JAD
CALL :KJX

GOTO :EOF

REM =======================


REM =======================
REM    MAKE BINARY FILE WITH PROGUARD

:PROGUARD

CALL :MF
CALL :BASE
CALL :PRO
CALL :PRE
CALL :JAR
CALL :JAD
CALL :KJX

GOTO :EOF

REM =======================


REM =======================
REM    MAKE BINARY FILE WITH RETROGUARD

:RETROGUARD

CALL :MF
CALL :BASE
CALL :RETRO
CALL :PRE
CALL :JAR
CALL :JAD
CALL :KJX

GOTO :EOF

REM =======================


REM =======================
REM    COMPILE

:BASE

DEL /Q classes\*
%JAVA_HOME%\bin\javac -g:none -d classes -bootclasspath %BOOT% -classpath %LIB% src\*.java

GOTO :EOF

REM =======================


REM =======================
REM    PREVERIFY 

:PRE

CD classes
FOR %%A IN (*.class) DO %PREVERIFY% -d . -classpath %BOOT%;%LIB%;. %%~nA
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    PROGUARD

:PRO

%JAVA_HOME%\bin\jar cvfM in.jar -C classes .
DEL /Q classes\*
%JAVA_HOME%\bin\java -jar %PROGUARD% -injar in.jar -outjar out.jar -libraryjars %BOOT%;%LIB% -keep class %CLASS% { public void startApp(); public void pauseApp(); public void destroyApp(); }
DEL in.jar
MOVE out.jar classes
CD classes
%JAVA_HOME%\bin\jar xf out.jar
DEL out.jar
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    RETROGUARD

:RETRO

%JAVA_HOME%\bin\jar cvfM in.jar -C classes .
DEL /Q classes\*
%JAVA_HOME%\bin\java -classpath %BOOT%;%LIB%;%RETROGUARD% RetroGuard
DEL in.jar
MOVE out.jar classes
CD classes
%JAVA_HOME%\bin\jar xf out.jar
DEL out.jar
RMDIR /S/Q META-INF
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    JAR COMPRESS

:JAR

%JAVA_HOME%\bin\jar cvfm bin\%JAR% bin\MANIFEST.MF -C classes .
%JAVA_HOME%\bin\jar uvfm bin\%JAR% bin\MANIFEST.MF -C res .

GOTO :EOF

REM =======================


REM =======================
REM    KJX TOOL

:KJX

%JAVA_HOME%\bin\java -jar %KJXTOOL% -c bin\%JAD% bin\%JAR% bin\%KJX%

GOTO :EOF

REM =======================


REM =======================
REM    MAKE JAVADOC

:DOC

%JAVA_HOME%\bin\javadoc -d doc -classpath %LIB% -private -author -version src\*.java

GOTO :EOF

REM =======================


REM =======================
REM    MAKE MANIFEST FILE

:MF

ECHO MIDlet-1: %APPLI%, /%ICON%, %CLASS% > bin\%MF%
REM ECHO MIDlet-Data-Size: %DSIZE% >> bin\%MF%
ECHO MIDlet-Description: %DESCRIPTION% >> bin\%MF%
ECHO MIDlet-Name: %APPLI% >> bin\%MF%
ECHO MIDlet-Version: %VERSION% >> bin\%MF%
ECHO MIDlet-Vendor: %VENDOR% >> bin\%MF%
ECHO MicroEdition-Configuration: CLDC-1.0 >> bin\%MF%
ECHO MicroEdition-Profile: MIDP-1.0 >> bin\%MF%

GOTO :EOF

REM =======================


REM =======================
REM    MAKE JAD FILE

:JAD

FOR %%A IN (bin\%JAR%) DO SET SIZE=%%~zA

ECHO MIDlet-1: %APPLI%, /%ICON%, %CLASS% > bin\%JAD%
ECHO MIDlet-Data-Size: %DSIZE% >> bin\%JAD%
ECHO MIDlet-Description: %DESCRIPTION% >> bin\%JAD%
ECHO MIDlet-Icon: /%ICON% >> bin\%JAD%
ECHO MIDlet-Jar-Size: %SIZE% >> bin\%JAD%
ECHO MIDlet-Jar-URL: %JAR% >> bin\%JAD%
ECHO MIDlet-Name: %APPLI% >> bin\%JAD%
ECHO MIDlet-Vendor: %VENDOR% >> bin\%JAD%
ECHO MIDlet-Version: %VERSION% >> bin\%JAD%
ECHO MicroEdition-Configuration: CLDC-1.0 >> bin\%JAD%
ECHO MicroEdition-Profile: MIDP-1.0 >> bin\%JAD%
ECHO MIDlet-Network: Y >> bin\%JAD%

GOTO :EOF

REM =======================

